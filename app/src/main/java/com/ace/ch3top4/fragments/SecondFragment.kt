package com.ace.ch3top4.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.ace.ch3top4.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val aName = arguments?.getString(FirstFragment.EXTRA_NAME)
        binding.tvName.text = "Your Name : $aName"

        binding.btnThirdFragment.setOnClickListener{ view ->
            if (binding.etName.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Input still empty", Toast.LENGTH_SHORT).show()
            } else {
                val actionToThirdFragment =
                    SecondFragmentDirections.actionSecondFragmentToThirdFragment()
                actionToThirdFragment.name = binding.etName.text.toString()
                view.findNavController().navigate(actionToThirdFragment)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}